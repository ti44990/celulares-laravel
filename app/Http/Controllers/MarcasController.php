<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Marcas;

class MarcasController extends Controller
{
   
    public function index()
    {
        $marcas = Marcas::all();
        return view ('marcas',compact('marcas'));
    }

   
    public function create()
    {
        //
    }

   
    public function store(Request $request)
    {
        $marca = new Marcas($request->input());
        $request->validate([
            'marca' => 'required|string|max:50'
        ]);
        $marca->saveOrFail();
        return redirect()->route('marcas.index')->with('success','Marca creada');
    }

   
    public function show(string $id)
    {
        $marca = Marcas::find($id);
        return view ('editMarca',compact('marca'));
    }

   
    public function edit(string $id)
    {
        //
    }

  
    public function update(Request $request, string $id)
    {
        $marca = Marcas::find($id);
        $request->validate([
            'marca' => 'required|string|max:50'
        ]);
        $marca->saveOrFail();
        $marca->fill($request->input())->saveOrFail();
        return redirect()->route('marcas.index')->with('success','Marca actualizada');
    }


    public function destroy(string $id)
    {
        $marca = Marcas::find($id);
        $marca->delete();
        return redirect()->route('marcas.index')->with('success','Marca eliminada');
    }
}

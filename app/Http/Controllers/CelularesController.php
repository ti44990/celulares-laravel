<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Celulares;
use App\Models\Marcas;
class CelularesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $celulares = Celulares::select('celulares.id','modelo','id_marca','marca','año','procecador','tamaño','precio')
        ->join('marcas','marcas.id','=','celulares.id_marca')->get();
        $marcas = Marcas::all();
        return view ('celulares',compact('celulares','marcas'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $celular = new Celulares($request->input());
        $request->validate([
            'modelo' => 'required|string',
            'id_marca' => 'required|numeric',
            'año' => 'required|date',
            'procecador' => 'required|string',
            'precio' => 'required|numeric',
            'tamaño' => 'required|numeric',
        ]);
        $celular->saveOrFail();
        return redirect()->route('celulares.index')->with('success','Celular creado');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $celular = Celulares::find($id);
        $marcas = Marcas::all();
        return view ('editCelular',compact('celular','marcas'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $celular = Celulares::find($id);
        $request->validate([
            'modelo' => 'required|string',
            'id_marca' => 'required|numeric',
            'año' => 'required|date',
            'procecador' => 'required|string',
            'tamaño' => 'required|numeric',
            'precio' => 'required|numeric',
        ]);
        $celular->fill($request->input())->saveOrFail();
        return redirect()->route('celulares.index')->with('success','Celular actualizado');
    }
    

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $celular = Celulares::find($id);
        $celular->delete();
        return redirect()->route('celulares.index')->with('success','Celular eliminado');
    }
    
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Celulares extends Model
{
    use HasFactory;
    public $timestamps = false; 
    protected $fillable= ['modelo','id_marca','año','procecador','tamaño','precio'];
}

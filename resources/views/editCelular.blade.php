@extends('plantilla')
@section('contenido')
<div class="row mt-3">
    <div class="col-md-6 offset-md-3">
        <div class="card">
            <div class="card-header bg-dark text-white">Editar Celular</div>
            <div class="card body">
                <form id="frmCelulares" method="POST" action="{{url("celulares",[$celular])}}">
                    @csrf
                    @method('PUT')
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-phone"></i></span>
                        <input type="text" name="modelo" value="{{ $celular->modelo}}" class="form-control" maxlength="50" placeholder="Modelo" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-globe"></i></span>
                        <select name="id_marca" class="form-select" required>
                            <option value="">Marca</option>
                            @foreach($marcas as $row)
                            @if($row->id == $celular->id_marca)
                                <option selected value="{{ $row->id}}">{{ $row->marca}}</option>
                            @else 
                                <option value="{{ $row->id}}">{{ $row->marca}}</option>
                            @endif

                            @endforeach
                        </select>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-calendar-day"></i></span>
                        <input type="date" name="año" value="{{ $celular->año}}" class="form-control" maxlength="50" placeholder="Año" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-microchip"></i></span>
                        <input type="text" name="procecador" value="{{ $celular->procecador}}" class="form-control" maxlength="50" placeholder="Procesador" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-up-right-and-down-left-from-center"></i></span>
                        <input type="number" name="tamaño" value="{{ $celular->tamaño}}" class="form-control" maxlength="50" placeholder="Tamaño" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-money-check-dollar"></i></i></span>
                        <input type="number" name="precio" value="{{ $celular->precio}}" class="form-control" maxlength="50" placeholder="Precio" required>
                    </div>

                    <div class="d-grid col-6 mx-auto">
                        <button class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i> Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
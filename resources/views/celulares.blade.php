@extends('plantilla')
@section('contenido')
@if ($mensaje = Session::get('success'))
   <div class="row mt-3" id="alertadiv">
    <div class="col-md-6 offset-md-3">
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <i class="fa-solid fa-check"></i> {{ $mensaje }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    </div>
   </div>
@endif

<div class="row mt-3">
    <div class="col-md-4 offset-md-4" >
        <div class="d-grid mx-auto">
            <button class="btn btn-info"  data-bs-toggle="modal" data-bs-target="#modalCelulares" >
                <i class="fa-solid fa-circle-plus" ></i> Añadir
            </button> 
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-12 col-lg-8 offset-0 offset-lg-2">
        <div class="table.-responsive" >
            <table class="table table-bordered table-hover" >
                <thead><tr><th>#</th><th>MODELO</th><th>MARCA</th><th>AÑO</th><th>PROCESADOR</th><th>TAMAÑO</th><th>PRECIO</th></tr></thead>
                <tbody class="table-group-divider" >
                    @php $i=1;@endphp
                    @foreach ($celulares as $row)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$row->modelo}}</td>
                            <td>{{$row->marca}}</td>
                            <td>{{$row->año}}</td>
                            <td>{{$row->procecador}}</td>
                            <td>{{$row->tamaño}}</td>
                            <td>{{$row->precio}}</td>
                            <td>
                                <a href="{{url('celulares',[$row])}}"class="btn btn-warning"><i class="fa-solid fa-edit" ></i></a>
                            </td>
                            <td>
                                <form method="POST" action="{{url('celulares',[$row])}}">
                                    @method("delete")
                                    @csrf
                                    <button class="btn btn-danger"><i class="fa-solid fa-trash" ></i></button>                               
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
    <div class="modal fade" id="modalCelulares" tabindex="-1" airia-hidden="true" >
        <div class="modal-dialog" >
            <div class="modal-content" >
                <div class="modal-header">
                    <label class="h5" id="titulo_modal" >Añadir Celular</label>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="frmCelulares" method="POST" action="{{url("celulares")}}">
                        @csrf
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-phone"></i></span>
                            <input type="text" name="modelo" class="form-control" maxlength="50" placeholder="Modelo" required>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-globe"></i></span>
                            <select name="id_marca" class="form-select">
                                <option value="">Marca</option>
                                @foreach($marcas as $row)
                                <option value="{{ $row->id}}">{{ $row->marca}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-calendar-day"></i></span>
                            <input type="date" name="año" class="form-control" maxlength="50" placeholder="Año" required>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-microchip"></i></span>
                            <input type="text" name="procecador" class="form-control" maxlength="50" placeholder="Procesador" required>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-up-right-and-down-left-from-center"></i></span>
                            <input type="number" name="tamaño" class="form-control" maxlength="50" placeholder="Tamaño" required>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-money-check-dollar"></i></span>
                            <input type="number" name="precio" class="form-control" maxlength="50" placeholder="Precio" required>
                        </div>

                        <div class="d-grid col-6 mx-auto">
                            <button class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i> Guardar</button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnCerrar" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
@vite('resources/js/listado.js')        
@endsection
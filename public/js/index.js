$(function(){
    getProductos();
});

var url = 'http://api.test/productsController.php';
function getProductos(){
    $('#contenido').empty();
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        success: function(respuesta){
            var productos = respuesta;
            if(productos.length > 0){
                jQuery.each(productos, function(i, prod){
                    var btnEditar = '<button class="btn btn-dark openModal" data-id="'+prod.id+'" data-universo="'+prod.Universo+'" data-nombre="'+prod.Nombre+'" data-identidad="'+prod.Identidad+'" data-historia="'+prod.Historia+'" data-apariciones="'+prod.Apariciones+'" data-op="2" data-bs-toggle="modal" data-bs-target="#modalProductos"><i class="fa-solid fa-pen"></i></button>';
                     var btnEliminar = '<button class="btn btn-danger delete" data-Id="'+prod.id+'" data-Nombre="'+prod.Nombre+'"><i class="fa-solid fa-trash"></i></button>';
                    $('#contenido').append('<tr><td>'+(i+1)+'</td><td>'+prod.Universo+'</td><td>'+prod.Nombre+'</td><td>'+prod.Identidad+'</td><td>'+prod.Historia+'</td><td>'+prod.Apariciones+'</td><td>'+btnEditar+'    '+btnEliminar+'</td></tr>');
                });
            }
        },
        error: function(){
            show_alerta('Error al mostrar los datos','error');
        }
    });
}

$(document).on('click', '#btnGuardar', function(){
	var Id = $('#Id').val();
	var Universo = $('#Universo').val().trim();
	var Nombre = $('#Nombre').val().trim();
    var Identidad = $('#Identidad').val().trim();
    var Historia = $('#Historia').val().trim();
    var Apariciones = $('#Apariciones').val().trim();
	var opcion = $('#btnGuardar').attr('data-operacion');
	var metodo, parametros;
	if(opcion == '1'){
		metodo="POST";
		parametros={Universo:Universo, Nombre:Nombre, Identidad:Identidad, Historia:Historia, Apariciones:Apariciones};

	}
	else{
		metodo="PUT";
		parametros={id:Id, Universo:Universo, Nombre:Nombre, Identidad:Identidad, Historia:Historia, Apariciones:Apariciones};
	}
	if(Universo === ''){
		show_alerta('Escribe el Universo al que pertenece', 'warning', 'Universo');
	}

	else if(Nombre === ''){
		show_alerta('Escribe el nombre', "warning", 'Nombre');

	}
	else if(Identidad==='' ){
		show_alerta('Escribe la Identidad', 'warning', 'Identidad');
	}
    else if(Historia==='' ){
		show_alerta('Escribe la Historia', 'warning', 'Historia');
	}
    else if(Apariciones==='' ){
		show_alerta('Escribe las apariciones', 'warning', 'Apariciones');
	}
	else{
		enviarSolicitud(metodo,parametros);
	}
});

function enviarSolicitud(metodo,parametros){
    $.ajax({
        type:metodo,
        url:url,
        data:JSON.stringify(parametros),
        dataType:'json',
        success: function(respuesta){
            show_alerta(respuesta[1],respuesta[0]);
            if(respuesta[0]=== 'success'){
                $('#btnCerrar').trigger('click');
                getProductos();
            }
        },
        error: function(){
            show_alerta('Error en la solicitud','error');
        }
    });
}

$(document).on('click','.openModal',function(){
    limpiar();
    var opcion = $(this).attr('data-op');
    if(opcion == '1'){
        $('#titulo_modal').html('Registrar Variable');
        $('#btnGuardar').attr('data-operacion', 1);
    } else {
        $('#titulo_modal').html('Editar variable');
        $('#btnGuardar').attr('data-operacion', 2);
        var Id= $(this).attr('data-id');
        var Universo= $(this).attr('data-universo');
        var Nombre= $(this).attr('data-nombre');
        var Identidad= $(this).attr('data-identidad');
        var Historia= $(this).attr('data-historia');
        var Apariciones= $(this).attr('data-apariciones');
        $('#Id').val(Id);
        $('#Universo').val(Universo);
        $('#Nombre').val(Nombre);
        $('#Identidad').val(Identidad);
        $('#Historia').val(Historia);
        $('#Apariciones').val(Apariciones);
    }
    window.setTimeout(function(){
        $('#Universo').trigger('focus');
    }, 500);
});


$(document).on('click', '.delete', function(){
    var Id = $(this).attr('data-Id');
    var nombre = $(this).attr('data-nombre');
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: { confirmButton: 'btn btn-success ms-3', cancelButton: 'btn btn-danger' },
        buttonsStyling: false
    });
    swalWithBootstrapButtons.fire({
        title: 'Seguro de eliminar la variante: ' + nombre,
        text: 'Se perderá la información permanentemente',
        icon: 'question',
        showCancelButton: true,
        confirmButtonText: 'Si, eliminar',
        cancelButtonText: 'Cancelar',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) {
            enviarSolicitud('DELETE', { id: Id });
        } else {
            show_alerta('La variante NO fue eliminada', 'error');
        }
    });
});


function limpiar(){
    $('#Id').val('');
    $('#Universo').val('');
    $('#Nombre').val('');
    $('#Identidad').val('');
    $('#Historia').val('');
    $('#Apariciones').val('')
}

function show_alerta(mensaje, icono, foco){
    if(foco !== ""){
        $('#'+foco).trigger('focus');
    }
    Swal.fire({
        title: mensaje,
        icon: icono,
        customClass: {confirmButton: 'btn btn-primary', popup: 'animated xoomIN'},
        buttonsStyling: false
    });
}

